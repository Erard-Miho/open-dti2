import { Component, EventEmitter, OnInit, Output, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { UserModel } from '../user.model';
import {NgbModal, ModalDismissReasons, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { FiredatabaseService } from '../core/firedatabase.service';
@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.scss']
})
export class EditprofileComponent implements OnInit, OnDestroy{

  editForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private modalService: NgbModal,
    private afDB:FiredatabaseService
  ){}

  private createForm():void{

    this.editForm = this.fb.group({

      nickname: new FormControl('',{
        validators:[Validators.maxLength(30)],
        updateOn: 'blur'
      }),

      name: new FormControl('', {
        validators: [Validators.maxLength(20), Validators.pattern('[a-zA-Z]*')],
        updateOn: 'blur'
      }),

      surname: new FormControl('', {
        validators: [Validators.maxLength(20), Validators.pattern('[a-zA-Z]*')],
        updateOn: 'blur'
      }),

      phone: new FormControl('', {
        validators: [Validators.pattern('[0-9]*')],
        updateOn: 'blur'
      }),

      address: new FormControl('', {
        validators: [Validators.maxLength(250)],
        updateOn: 'blur'
      })
    })
  }

   onSubmit(): void{
    
    if (this.editForm.valid){
      const {nickname, name, surname,phone, address} = this.editForm.value;

      this.afDB.createUserData(nickname,name,surname,phone,address)
    }
    this.close();
    alert('Profile Edited')
  }
 ngOnInit(){
  this.createForm();
 }

 close():void{
   this.modalService.dismissAll()
 }

 ngOnDestroy():void{
   this.close();
 }
}
