import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ModuleWithProviders } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AngularFireModule } from '@angular/fire';
import { environment } from './../environments/environment';
import { PopupComponent } from './popup/popup.component';
import { EditprofileComponent } from './editprofile/editprofile.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UpdateEmailPasswordComponent } from './update-email-password/update-email-password.component';
import { PopupChangeMailPassComponent } from './popup-change-mail-pass/popup-change-mail-pass.component';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
// List of providers
const providers = [];

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    PopupComponent,
    EditprofileComponent,
    UpdateEmailPasswordComponent,
    PopupChangeMailPassComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    FormsModule,
    ReactiveFormsModule,
    NgbModalModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }


@NgModule({})
export class UserProfileSharedModule {
  static forRoot(): ModuleWithProviders<UserProfileSharedModule> {
    return {
      ngModule: AppModule,
      providers
    };
  }
}
