import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-popup-change-mail-pass',
  templateUrl: './popup-change-mail-pass.component.html',
  styleUrls: ['./popup-change-mail-pass.component.scss']
})
export class PopupChangeMailPassComponent implements OnInit {

  constructor(private modalService: NgbModal) { }


  closeResult = '';
  open(content) {

    this.modalService.open(content, {centered: true, size: 'lg'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;

    });

  }
  private getDismissReason(reason: any): string {

    if (reason === ModalDismissReasons.ESC) {

      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {

      return 'by clicking on a backdrop';
    }
     else {
      return `with: ${reason}`;
    }
  }





  ngOnInit(): void {
  }

}
