import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupChangeMailPassComponent } from './popup-change-mail-pass.component';

describe('PopupChangeMailPassComponent', () => {
  let component: PopupChangeMailPassComponent;
  let fixture: ComponentFixture<PopupChangeMailPassComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PopupChangeMailPassComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupChangeMailPassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
