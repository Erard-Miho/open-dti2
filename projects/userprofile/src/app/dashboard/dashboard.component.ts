import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { forkJoin } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { PopupComponent } from '../popup/popup.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private afauth: AngularFireAuth) { }

  @ViewChild("popup") PopUp: PopupComponent;

  userValues$ = this.afauth.currentUser.then((user)=>{
    // get user details
    if(user){
    return [{
      "id":user.uid,
      "email":user.email,
      "nickname": "user nickname",
      "name":"user name",
      "surname":"user surname",
      "phone": "user phone",
      "address": "user address"
      
    }]
  }
}).catch(error =>{
    // catch error if null
    let err = "";
    err = 'Error - ' + error.message

  })

  open(content){
    this.PopUp.open(content);
  }
  ngOnInit(){

  }

}
