export interface UserModel {
    id?: string;
    nickname:string;
    name: string;
    surname: string;
    email: number;
    phone: string;
    address: string;
  }
