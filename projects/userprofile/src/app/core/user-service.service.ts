import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CredentialInfo } from 'src/app/core/models/credential-info';
import * as firebase from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(private afAuth: AngularFireAuth) { }

  login(request: CredentialInfo): Promise<firebase.default.auth.UserCredential> {
    return this.afAuth.signInWithEmailAndPassword(request?.email, request?.password).catch();
  }
}
