import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import * as firebase from 'firebase';
import { CredentialInfo } from 'src/app/core/models/credential-info';

@Injectable({
  providedIn: 'root'
})
export class FiredatabaseService {

  constructor(private afDB: AngularFireDatabase, private afAuth: AngularFireAuth) { }
  changeEmail(email){
    // user must login with email and password
    // i will do this by making a popup when the user click on the email to login
    // should i do this using an observable ? 
    // if field of email is clicked popup email sign in
    
  }
  createUserData(nickname,name,surname,phone,address){
    let user = firebase.default.auth().currentUser.uid;
    
        this.afDB.database.ref(`userDetails/${user}`).child('user').set(
          {
          "id":user,
          "nickname":nickname,
          "name":name,
          "surname":surname,
          "phone":phone,
          "address":address
        })
  }
}
