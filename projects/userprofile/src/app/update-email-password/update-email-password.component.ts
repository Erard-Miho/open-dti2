import { Component, OnDestroy, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as firebase from 'firebase';

@Component({
  selector: 'app-update-email-password',
  templateUrl: './update-email-password.component.html',
  styleUrls: ['./update-email-password.component.scss']
})
export class UpdateEmailPasswordComponent implements OnInit, OnDestroy {

  updateForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private modalService: NgbModal,
    private afAuth: AngularFireAuth
  ) { }

  private createForm():void{

    this.updateForm = this.fb.group({

      youremail: new FormControl('',{
        validators: [Validators.required, Validators.email]
      }),

      yourpass: new FormControl('', {
        validators: [Validators.required]
      }),

      newemail: new FormControl('',{
        validators:[Validators.email],
        updateOn: 'blur'
      }),

      newpass: new FormControl('',{
        validators: [Validators.minLength(8)],
        updateOn: 'blur'
      })

    })
  }

  oldemail="";
  oldpass="";

  onSubmit(): void{
    if(this.updateForm.valid){
      let user = firebase.default.auth().currentUser;
      this.afAuth.signInWithEmailAndPassword(this.oldemail, this.oldpass).then(_ =>{
      user.updateEmail(this.updateForm.controls.newemail.value).then(_ =>{ user.updatePassword(this.updateForm.controls.newpass.value).catch(error =>{console.log(error)});  }).catch(err=>{console.log(err)})
      
    })
    }
  }

  close():void{
    this.modalService.dismissAll();
  }

  ngOnDestroy():void{
    this.modalService.dismissAll();
  }

  ngOnInit(): void {
    this.createForm();
  }

}
