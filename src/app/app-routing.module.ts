import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FullLayoutComponent } from './layout/full-layout/full-layout.component';
import { GuardGuard } from './core/guard/guard.guard';
import { ErrorComponent } from './shared/error/error.component';
import { UserProfileSharedModule } from '../../projects/userprofile/src/app/app.module';

const routes: Routes = [
  {
    path:'home',
    component:FullLayoutComponent,
    children:[
      { path:'',
        loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule)
    }
    ]
  },
  {
    path:'',
    redirectTo:'home',
    pathMatch:'full'
  }


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    UserProfileSharedModule.forRoot()
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
