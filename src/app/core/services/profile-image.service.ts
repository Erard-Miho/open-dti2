import { Injectable } from '@angular/core';
import {iif, Observable} from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import {
  AngularFireDatabase,
  AngularFireList,
  SnapshotAction
} from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';
import { catchError, map, switchMap, takeLast } from 'rxjs/operators';
import { ToastrService } from 'src/app/core/services/toastr.service';
import { BaseHttpService } from './base-http';
@Injectable({
  providedIn: 'root'
})
export class ProfileImageService extends BaseHttpService{

  constructor(
    private db: AngularFireDatabase,
    private afAuth: AngularFireAuth,
    toastr: ToastrService,
    private afStorage: AngularFireStorage
  ) {
    super(toastr);
   }

   getImage():Observable<any>{
     return this.afAuth.authState.pipe(
      switchMap((user) =>
        iif(
          () => !!user?.uid,
          (() => {
           let thisdb = this.db.list(`userDetails/${user?.uid}/user`, (rf) =>
              rf.orderByChild('profileImage')
            );
            return thisdb.snapshotChanges();
          })()
        )
      ),
      catchError(this.catchError.bind(this))
     );
   }

   getPhoto(){
     this.afAuth.currentUser.then((user) =>{
       this.db.list(`userDetails/${user?.uid}/user`, (rf) => rf.orderByChild('profileImage')).snapshotChanges.prototype.payload
     })
   }



}
