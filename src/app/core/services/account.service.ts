import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { CredentialInfo } from '../models/credential-info';
import firebase from 'firebase/app';
import { BaseHttpService } from './base-http.service';
import { ToastrService } from './toastr.service';
import {AngularFireStorage} from '@angular/fire/storage'
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class AccountService extends BaseHttpService {

  constructor(private afAuth: AngularFireAuth, toastr: ToastrService, private afStorage: AngularFireStorage) {
    super(toastr);
   }
  
   register(request: CredentialInfo): Promise<firebase.auth.UserCredential>{
     return this.afAuth.createUserWithEmailAndPassword(request?.email, request?.password).catch(this.catchError.bind(this));
   }
  
   login(request: CredentialInfo): Promise<firebase.auth.UserCredential> {
     return this.afAuth.signInWithEmailAndPassword(request?.email, request?.password).catch(this.catchError.bind(this));
   }
  
   logout(): Promise<void> {
     return this.afAuth.signOut();
   }

   getPhoto(): Observable<any>{
     var uid = "";
     var useri = this.afAuth.currentUser.then(id => {uid = id.uid});
     return this.afStorage.ref('images/profile/image'+uid).getDownloadURL()
   }
   
   
}
