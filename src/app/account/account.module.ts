import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { LayoutModule } from '../layout/layout.module';
import { PagesModule } from '../pages/pages.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AccountRoutingModule } from './account-routing.module';
import { SharedModule } from '../shared/shared.module';
import { RouterModule} from '@angular/router';
import { UserModule } from './user/user.module';

@NgModule({
  declarations: [LoginComponent, RegisterComponent],
  imports: [
    CommonModule,
    LayoutModule,
    PagesModule,
    ReactiveFormsModule,
    AccountRoutingModule,
    SharedModule,
    UserModule,
    RouterModule,
  
  ]
})
export class AccountModule { }
