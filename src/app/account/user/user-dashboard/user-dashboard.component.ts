import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { NgbModal,  ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AccountService } from 'src/app/core/services/account.service';
import { AngularFireStorage } from '@angular/fire/storage';
import { ProfileImageService } from 'src/app/core/services/profile-image.service';
import { switchMap } from 'rxjs/operators';
import { forkJoin, iif, Observable } from 'rxjs';
import { AngularFireDatabase } from '@angular/fire/database';
import { stringify } from '@angular/compiler/src/util';


@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.scss']
})
export class UserDashboardComponent implements OnInit {
  

  constructor(private router: Router, private accountService: AccountService, 
    private afAuth:AngularFireAuth, private modalService: NgbModal, 
    private http: HttpClient, private afStorage: AngularFireStorage,
    private photo: ProfileImageService, private afDB: AngularFireDatabase) { 
      
      
     
    }

  closeResult = '';
 
  profileImageUrl = "";

  ngOnInit(): void {
    this.showImage()
  }

  showImage(){
    this.afAuth.currentUser.then((user)=>{
      this.afStorage.ref(`images/profile/user/${user.uid}`).getDownloadURL().subscribe(m=>{
        this.profileImageUrl = m
      })
    
    })
  }


  logout(): void {
    this.accountService.logout();
    localStorage.clear();
    this.router.navigate(['/']);
  }
///////////////////////////////////// To pop up the image input


open(content) {
  
  this.modalService.open(content, {centered: true}).result.then((result) => {
    this.closeResult = `Closed with: ${result}`;

  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;

  });

}
private getDismissReason(reason: any): string {
  
  if (reason === ModalDismissReasons.ESC) {
   
    return 'by pressing ESC';
  } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
   
    return 'by clicking on a backdrop';
  }
   else {
    return `with: ${reason}`;
  }
}
// to upload the image
selectedFile: File = null;
  onFileSelected(event){
    this.selectedFile = <File>event.target.files[0];
    console.log(this.selectedFile.type.split('/')[1])
  }

  
//////////////////////////////////////////////////

/// Other options to upload image and show 

bothStorageAndDB(userID:any):Observable<any>{
  const fd = new FormData(); // file format
  fd.append('image', this.selectedFile, this.selectedFile.name);

    //storage
    let storage = this.afStorage.ref(`images/profile/user/${userID}`).put(
      fd.get('image')
    ).then(() =>{console.log("uploaded")});
  
    // get storage url we subcribe so we take url as string
    let storageURL = this.afStorage.ref(`images/profile/user/${userID}`).getDownloadURL().subscribe(m =>{

    // DB
    this.afDB.database.ref(`userDetails/${userID}`).child('user').set({
      "id":userID,
      "profileImage": m 
    });
  })
  return forkJoin([storage, storageURL])
}

// upload photo to storage and database
onUpload(){

  this.afAuth.currentUser.then((user) =>{
    this.bothStorageAndDB(user.uid);
    this.modalService.dismissAll()
  })
}

}
