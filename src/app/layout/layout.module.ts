import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FullLayoutComponent } from './full-layout/full-layout.component';
import { RouterModule } from '@angular/router';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { FooterComponent } from './footer/footer.component';


@NgModule({
  declarations: [FullLayoutComponent, NavBarComponent, FooterComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports:[
    FullLayoutComponent,
    NavBarComponent,
    FooterComponent
  ]
})
export class LayoutModule { }
