import { Component, Input, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AccountService } from 'src/app/core/services/account.service';
import firebase from 'firebase/app';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  constructor(private router: Router, private accountService: AccountService, private fireauth: AngularFireAuth) { 
    
    
  }
  
  check=false;
  
  changeApp(){
    this.router.navigate(['dashboard'])
  }
  
  logout(){
    this.accountService.logout();
    alert('You have logged out');
  }



  ngOnInit(){
    this.fireauth.onAuthStateChanged(user => {
      if(user) {
        this.check = true;
      } else {
        this.check = false;
      }
    });
  }

  

}
