import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionRecommendedItemsComponent } from './section-recommended-items.component';

describe('SectionRecommendedItemsComponent', () => {
  let component: SectionRecommendedItemsComponent;
  let fixture: ComponentFixture<SectionRecommendedItemsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionRecommendedItemsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionRecommendedItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
