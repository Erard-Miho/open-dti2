import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionTradeServiceComponent } from './section-trade-service.component';

describe('SectionTradeServiceComponent', () => {
  let component: SectionTradeServiceComponent;
  let fixture: ComponentFixture<SectionTradeServiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionTradeServiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionTradeServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
