import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionRegionComponent } from './section-region.component';

describe('SectionRegionComponent', () => {
  let component: SectionRegionComponent;
  let fixture: ComponentFixture<SectionRegionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionRegionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionRegionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
