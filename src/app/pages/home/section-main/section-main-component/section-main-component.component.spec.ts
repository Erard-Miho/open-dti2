import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionMainComponentComponent } from './section-main-component.component';

describe('SectionMainComponentComponent', () => {
  let component: SectionMainComponentComponent;
  let fixture: ComponentFixture<SectionMainComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionMainComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionMainComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
