import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionDealComponentComponent } from './section-deal-component.component';

describe('SectionDealComponentComponent', () => {
  let component: SectionDealComponentComponent;
  let fixture: ComponentFixture<SectionDealComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionDealComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionDealComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
