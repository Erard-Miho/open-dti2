import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionRequestQuotationComponent } from './section-request-quotation.component';

describe('SectionRequestQuotationComponent', () => {
  let component: SectionRequestQuotationComponent;
  let fixture: ComponentFixture<SectionRequestQuotationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionRequestQuotationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionRequestQuotationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
