import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionApparelComponent } from './section-apparel.component';

describe('SectionApparelComponent', () => {
  let component: SectionApparelComponent;
  let fixture: ComponentFixture<SectionApparelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionApparelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionApparelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
