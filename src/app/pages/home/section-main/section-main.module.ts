import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SectionMainComponentComponent } from './section-main-component/section-main-component.component';
import { RouterModule} from '@angular/router';
import { SectionDealComponentComponent } from './section-deal-component/section-deal-component.component';
import { SectionApparelComponent } from './section-apparel/section-apparel.component';
import { SectionElectronicsComponent } from './section-electronics/section-electronics.component';
import { SectionRequestQuotationComponent } from './section-request-quotation/section-request-quotation.component';
import { SectionRecommendedItemsComponent } from './section-recommended-items/section-recommended-items.component';
import { SectionTradeServiceComponent } from './section-trade-service/section-trade-service.component';
import { SectionRegionComponent } from './section-region/section-region.component';
import { SectionSubscribeComponent } from './section-subscribe/section-subscribe.component';


@NgModule({
  declarations: [SectionMainComponentComponent, SectionDealComponentComponent, SectionApparelComponent, SectionElectronicsComponent, SectionRequestQuotationComponent, SectionRecommendedItemsComponent, SectionTradeServiceComponent, SectionRegionComponent, SectionSubscribeComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports:[
    SectionMainComponentComponent,
    SectionDealComponentComponent,
    SectionApparelComponent,
    SectionElectronicsComponent,
    SectionRequestQuotationComponent,
    SectionRecommendedItemsComponent,
    SectionTradeServiceComponent,
    SectionRegionComponent,
    SectionSubscribeComponent
  ]
})
export class SectionMainModule { }
