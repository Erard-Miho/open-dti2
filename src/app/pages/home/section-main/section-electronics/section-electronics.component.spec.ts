import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionElectronicsComponent } from './section-electronics.component';

describe('SectionElectronicsComponent', () => {
  let component: SectionElectronicsComponent;
  let fixture: ComponentFixture<SectionElectronicsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionElectronicsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionElectronicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
