import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartProductsComponent } from './cart-products/cart-products.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { RouterModule } from '@angular/router';




@NgModule({
  declarations: [CartProductsComponent, CheckoutComponent],
  imports: [
    CommonModule,
    RouterModule,
   
  ],
  exports:[
    CartProductsComponent,
    CheckoutComponent,
    CartProductsComponent
  ]
})
export class CartModuleModule { }
