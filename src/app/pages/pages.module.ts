import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { HomeComponent } from './home/home.component';
import { SingleProductComponent } from './single-product/single-product.component';
import { CartComponent } from './cart/cart.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductsModule } from '../products/products.module';
import {SectionMainModule} from './home/section-main/section-main.module';
import { CartModuleModule } from './cart/cart-module.module';

@NgModule({
  declarations: [HomeComponent, SingleProductComponent, CartComponent, ProductListComponent],
  imports: [
    CommonModule,
    PagesRoutingModule,
    ProductsModule,
    SectionMainModule,
    CartModuleModule
    
  ],
  exports:[
    HomeComponent
  ]
})
export class PagesModule { }
