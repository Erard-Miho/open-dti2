import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GuardGuard } from '../core/guard/guard.guard';
import { CartComponent } from './cart/cart.component';
import { CheckoutComponent } from './cart/checkout/checkout.component';
import { HomeComponent } from './home/home.component';
import { ProductListComponent } from './product-list/product-list.component';
import { SingleProductComponent } from './single-product/single-product.component';

const routes: Routes = [
  {
    path:'',
    component: HomeComponent
  },
  {
    path:'single-product',
    component:SingleProductComponent
  },
  {
    path:'cart',
    component:CartComponent
  },
  {
    path:'product-list',
    component: ProductListComponent
  },
  {
    path:'checkout',
    canActivate:[GuardGuard],
    component:CheckoutComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
