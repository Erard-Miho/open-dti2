import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductCategoryComponent } from './product-category/product-category.component';
import { RouterModule} from '@angular/router';

@NgModule({
  declarations: [ProductCategoryComponent],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    RouterModule
  ],
  exports:[
    ProductCategoryComponent
  ]

})
export class ProductsModule { }
