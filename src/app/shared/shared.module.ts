import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerOverlayComponent } from './spinner-overlay/spinner-overlay.component';
import { ToastrComponent } from './toastr/toastr.component';
import { ErrorComponent } from './error/error.component';




@NgModule({
  declarations: [SpinnerOverlayComponent, ToastrComponent, ErrorComponent],
  imports: [
    CommonModule
  ],
  exports:[
    SpinnerOverlayComponent,
    ToastrComponent
  ]
})
export class SharedModule { }
